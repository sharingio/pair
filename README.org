#+TITLE: Pair

#+begin_quote
Pairing is sharing
#+end_quote

* Features
- provision sharable pairing environment
- build out of your project
- instance development and project set up
- get a Kubeconfig and a tmate session to begin

For a full rundown of features, see the [[./org/features.org][features docs]].

* Set up
For setting up Pair, check out the [[./org/deployment.org][deployment docs]] and the [[./org/configuration.org][configuration docs]].

* Connecting
Using [[./hack/pair-ssh-instance][pair-ssh-instance]], you are able to connect easily to your instance via SSH.
#+BEGIN_SRC shell
pair-ssh-instance <instance name>
#+END_SRC

Note:
- will forward your SSH keys
- will attach to the tmate session

* Other things to see
- Instance Pairing environment: [[https://github.com/sharingio/environment][sharingio/environment]]
- Instance configuration: [[https://github.com/sharingio/.sharing.io][.sharing.io]]

* License

Copyright 2020-2022 Sharingio authors and contributors.

Pair is license under the Apache 2.0 license.
