package routes

import (
	"net/http"

	"github.com/sharingio/pair/apps/cluster-api-manager/types"

	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// GetEndpoints ...
// returns endpoints to register
func GetEndpoints(endpointPrefix string, clientset *kubernetes.Clientset, dynamicClient dynamic.Interface, restConfig *rest.Config) types.Endpoints {
	return types.Endpoints{

		// swagger:route GET /hello hello getHello
		//
		// Say hello
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: metaResponse
		{
			EndpointPath: endpointPrefix + "/hello",
			HandlerFunc:  GetAPIHello,
			HTTPMethods:  []string{http.MethodGet},
		},

		// swagger:route GET /teapot teapot getTeapot
		//
		// I'm a little teapot
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       418: metaResponse
		{
			EndpointPath: endpointPrefix + "/teapot",
			HandlerFunc:  GetTeapot,
			HTTPMethods:  []string{http.MethodGet},
		},

		// swagger:route GET /instance instance listInstances
		//
		// List all instances
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: instanceList
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance",
			HandlerFunc:  ListInstances(dynamicClient, clientset),
			HTTPMethods:  []string{http.MethodGet},
		},

		// swagger:route GET /instance/kubernetes instance listInstancesKubernetes
		//
		// List all Kubernetes instances
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: instanceList
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance/kubernetes",
			HandlerFunc:  ListInstancesKubernetes(dynamicClient, clientset),
			HTTPMethods:  []string{http.MethodGet},
		},

		// swagger:route GET /instance/kubernetes/{name} instance getInstanceKubernetes
		//
		// get a Kubernetes instance
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: instance
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance/kubernetes/{name}",
			HandlerFunc:  GetInstanceKubernetes(dynamicClient, clientset),
			HTTPMethods:  []string{http.MethodGet},
		},

		// swagger:route GET /instance/kubernetes/{name}/kubeconfig instance getInstanceKubernetesKubeconfig
		//
		// get a kubeconfig for a Kubernetes instance
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: instanceData
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance/kubernetes/{name}/kubeconfig",
			HandlerFunc:  GetKubernetesKubeconfig(clientset),
			HTTPMethods:  []string{http.MethodGet},
		},

		// swagger:route GET /instance/kubernetes/{name}/ingresses instance getInstanceKubernetesIngresses
		//
		// get available ingresses for a Kubernetes instance
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: instanceIngresses
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance/kubernetes/{name}/ingresses",
			HandlerFunc:  GetKubernetesIngresses(clientset),
			HTTPMethods:  []string{http.MethodGet},
		},

		// swagger:route POST /instance/kubernetes/{name}/certmanage instance getInstanceKubernetesCertmanage
		//
		// initiate certificate management for an instance
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: metaResponse
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance/kubernetes/{name}/certmanage",
			HandlerFunc:  PostKubernetesCertManage(clientset, dynamicClient),
			HTTPMethods:  []string{http.MethodGet, http.MethodPost},
		},

		// swagger:route POST /instance/kubernetes/{name}/dnsmanage instance getInstanceKubernetesDNSmanage
		//
		// initiate DNS management for an instance
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: metaResponse
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance/kubernetes/{name}/dnsmanage",
			HandlerFunc:  PostKubernetesDNSManage(dynamicClient, clientset),
			HTTPMethods:  []string{http.MethodGet, http.MethodPost},
		},

		// swagger:route GET /instance/kubernetes/{name}/tmate instance getInstanceKubernetesTmate
		//
		// get a tmate SSH sesion for an instance
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: instanceData
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance/kubernetes/{name}/tmate",
			HandlerFunc:  GetKubernetesTmateSSHSession(clientset, restConfig, dynamicClient),
			HTTPMethods:  []string{http.MethodGet},
		},

		// swagger:route GET /instance/kubernetes/{name}/tmate/ssh instance getInstanceKubernetesTmateSSH
		//
		// get a tmate SSH sesion for an instance
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: instanceData
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance/kubernetes/{name}/tmate/ssh",
			HandlerFunc:  GetKubernetesTmateSSHSession(clientset, restConfig, dynamicClient),
			HTTPMethods:  []string{http.MethodGet},
		},

		// swagger:route GET /instance/kubernetes/{name}/tmate/web instance getInstanceKubernetesTmateWeb
		//
		// get a tmate web sesion for an instance
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: instanceData
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance/kubernetes/{name}/tmate/web",
			HandlerFunc:  GetKubernetesTmateWebSession(clientset, restConfig, dynamicClient),
			HTTPMethods:  []string{http.MethodGet},
		},

		// swagger:route POST /instance instance postInstance
		//
		// creates an instance
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: instance
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance",
			HandlerFunc:  PostInstance(dynamicClient, clientset),
			HTTPMethods:  []string{http.MethodPost},
		},

		// swagger:route DELETE /instance/kubernetes/{name} instance deleteInstanceKubernetes
		//
		// delete a Kubernetes instance
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: instance
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance/kubernetes/{name}",
			HandlerFunc:  DeleteInstanceKubernetes(dynamicClient, clientset),
			HTTPMethods:  []string{http.MethodDelete},
		},

		// swagger:route DELETE /instance instance deleteInstance
		//
		// delete an instance
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: instance
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance",
			HandlerFunc:  DeleteInstance(dynamicClient),
			HTTPMethods:  []string{http.MethodDelete},
		},

		// swagger:route POST /instance/kubernetes/{name}/syncProviderID instance updateInstanceKubernetesNodeProviderID
		//
		// update ProviderID on Instance Nodes
		//
		//     Consumes:
		//     - application/json
		//
		//     Produces:
		//     - application/json
		//
		//     Schemes: http
		//
		//     Responses:
		//       200: metaResponse
		//       500: failure
		{
			EndpointPath: endpointPrefix + "/instance/kubernetes/{name}/syncProviderID",
			HandlerFunc:  PostKubernetesUpdateInstanceNodeProviderID(clientset, dynamicClient),
			HTTPMethods:  []string{http.MethodGet, http.MethodPost},
		},
	}
}
